"use strict";

// IMPORTS

const fs = require('fs-extra');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');

// CONFIG

const config = {
	keys: {
		'username': 'your_secret_pass'
	},
	production : 0,
	base: "/home/pi/PUBLIC",
	port: 8888,
	cmode: "0755"
}

const server = express();
server.use(express.static('public'))
server.use(bodyParser.json());

const sess = {
	secret: 'keyboard cat',
	saveUninitialized : false,
	cookie: {}
}

if(config.production) {
	server.set('trust proxy', 1);
	sess.cookie.secure = true;
}

server.use(session(sess));

// Start Server

server.listen(config.port, function () {
	console.log("%s listening at port %s", server.name, config.port);
});

var merge = function (obj1, obj2) {
	var mobj = {},
		attrname;
	for (attrname in obj1) {
		mobj[attrname] = obj1[attrname];
	}
	for (attrname in obj2) {
		mobj[attrname] = obj2[attrname];
	}
	return mobj;
};

var resError = function (code, raw, res) {

	var codes = {
		100: "Unknown command",
		101: "Could not list files",
		102: "Could not read file",
		103: "Path does not exist",
		104: "Could not create copy",
		105: "File does not exist",
		106: "Not a file",
		107: "Could not write to file",
		108: "Could not delete object",
		404: "Not Authorized"
	};

	res.json({
		"status": "error",
		"code": code,
		"message": codes[code],
		"raw": raw
	});

};

/**
 * Get Base Path
 */

var getBasePath = function (path) {
	var base_path = path.split("/");

	base_path.pop();
	return base_path.join("/");
};

/**
 * Check Path
 */

var checkPath = function (path) {
	var base_path = getBasePath(path);
	return fs.existsSync(base_path);
};



// Login

server.post('/session/login', function (req, res) {

	console.log(req.body);

	if(config.keys[req.body.username] !== req.body.password) {
		console.log("nope");
		return res.json({
			status: 'failure'
		});
	}

	console.log("okay");

	req.session.login = 1;
	res.json({
		status: 'success'
	});

});

server.post('/session/logout', function (req, res) {

	req.session.destroy(function(err) {
		res.json({
			status: 'success'
		});
	});

});

server.post("/session/list/", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;
	fs.readdir(path, function (err, files) {
		if (err) {
			return resError(101, err, res);
		}

		// Ensure ending slash on path
		(path.slice(-1) !== "/") ? path = path + "/": path = path;

		var output = {},
			output_dirs = {},
			output_files = {},
			current,
			relpath,
			link;

		// Function to build item for output objects 
		var createItem = function (current, relpath, type, link) {
			return {
				path: relpath.replace('//', '/'),
				type: type,
				size: fs.lstatSync(current).size,
				atime: fs.lstatSync(current).atime.getTime(),
				mtime: fs.lstatSync(current).mtime.getTime(),
				link: link
			};
		};

		// Sort alphabetically
		files.sort();

		// Loop through and create two objects
		// 1. Directories
		// 2. Files
		for (var i = 0, z = files.length - 1; i <= z; i++) {
			current = path + files[i];
			relpath = current.replace(config.base, "");
			(fs.lstatSync(current).isSymbolicLink()) ? link = true: link = false;
			if (fs.lstatSync(current).isDirectory()) {
				output_dirs[files[i]] = createItem(current, relpath, "directory", link);
			} else {
				output_files[files[i]] = createItem(current, relpath, "file", link);
			}
		}

		// Merge so we end up with alphabetical directories, then files
		output = merge(output_dirs, output_files);

		// Send output
		res.json(output);

	});


});

server.post("/session/file/", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;
	fs.readFile(path, "utf8", function (err, data) {
		if (err) {
			return resError(102, err, res);
		}

		res.json({
			status: "success",
			data: data
		});

	});

});

server.post("/session/save/", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;
	// Make sure it exists
	if (!fs.existsSync(path)) {
		return resError(105, null, res);
	}

	// Make sure it's a file
	if (fs.lstatSync(path).isDirectory()) {
		return resError(106, null, res);
	}

	// Write (binary encoding supports text and binary files)
	fs.writeFile(path, req.body.data, 'binary', function (err) {
		if (err) {
			return resError(107, err, res);
		}

		res.json({
			status: 'success'
		});
	});

});

server.post("/session/create/", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;
	if (!checkPath(path)) {
		return resError(103, null, res);
	}

	switch (req.body.type) {
	case "dir":

		// Base path exists, create directory
		fs.mkdir(path, config.cmode, function () {
			res.json({
				status: 'success'
			});
		});
		break;
	case "file":

		// Base path exists, create file
		fs.openSync(path, "w");
		res.json({
			status: 'success'
		});

		break;
	}


});

server.post("/session/rename", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;
	let base_path = getBasePath(path);

	fs.rename(path, base_path + "/" + req.body.name, function () {
		res.json({
			'status': 'success'
		});
	});

});

server.post("/session/delete", async function (req, res) {

	if(!req.session.login) {
		return resError(403, null, res);
	}

	let path = config.base + req.body.path;

	// Make sure it exists
	if (!fs.existsSync(path)) {
		return resError(103, null, res);
	}

	// Remove file or directory
	fs.remove(path, function (err) {
		if (err) {
			return resError(108, err, res);
		}
		res.json({
			'status': 'success'
		});
	});

});
